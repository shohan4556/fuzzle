Fuzzle.HowTo = function (game) {
    this.game = game;
}


Fuzzle.HowTo.prototype.create = function () {
    //console.log(this.game.state.getCurrentState());

    this.game.stage.backgroundColor = '#FFFFFF'; // set background color white 
    // tutorial sprite added 
    var sprite = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'howto_01');
    // set anchor point to center
    sprite.anchor.setTo(0.5);
    // scale sprite
    sprite.scale.setTo(this.game.global.howToXY);
    // enable tap 
    sprite.inputEnabled = true;
    // when tapped add an event listener
    sprite.events.onInputDown.add(this.NEXT_howTo, this);

}

Fuzzle.HowTo.prototype.NEXT_howTo = function (sprite_01) {
    sprite_01.destroy(); // destroy the pervious sprite 
    
    // add new tutorial sprite
    var sprite = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'howto_02');
    // set anchor point to center
    sprite.anchor.setTo(0.5);
    sprite.scale.setTo(this.game.global.howToXY);
    sprite.inputEnabled = true;
    // when tapped add an event listener
    sprite.events.onInputDown.add(this.NEXT_STATE, this);
}

Fuzzle.HowTo.prototype.NEXT_STATE = function () {
    this.game.state.clearCurrentState(); // crelear this state

    this.game.state.start('Play');
}