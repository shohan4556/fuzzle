var Widget = function (game) {
    this.game = game;
    this.aw = this.game.global.assetWidth;
    this.ah = this.game.global.assetHeight;

    this.timeUp = false;
    this.slowMp = false;
}


Widget.prototype.create = function (_GameTime) {
    this.GameTime = _GameTime; // store the reference

    var randTime = this.game.rnd.integerInRange(15, 35);

    this.Time = this.game.time.create(false); // auto destroy false it will run forever until the timer is stopped 
    this.Time.loop(Phaser.Timer.SECOND * 10, this.createSprite, this);
    this.Time.start();
}

// create timer sprite
Widget.prototype.createSprite = function () {

    var scoreStyle = {
        font: "30px Oswald",
        fill: '#FF790D',
        strokeThickness: 10
    };

    var sprite = this.game.add.text(this.game.world.centerX, this.game.world.centerY, 'Time++', scoreStyle);
    sprite.anchor.setTo(0.5);
    sprite.scale.setTo(this.aw, this.ah);
    this.game.physics.arcade.enable(sprite); // enabled physics
    sprite.inputEnabled = true; // enabled input
    sprite.checkWorldBounds = true; // check when cross the world 
    sprite.outOfBoundsKill = true; // kill when crossed the game world
    
    // Time++ velocity here
    if (window.devicePixelRatio <= 1.5) {
        var randomX = this.game.rnd.integerInRange(-150, 150);
        var randomY = this.game.rnd.integerInRange(-150, 150);
    } else {
        var randomX = this.game.rnd.integerInRange(-300, 300);
        var randomY = this.game.rnd.integerInRange(-300, 300);
    }

    sprite.body.velocity.setTo(randomX, randomY);

    sprite.events.onInputDown.add(this.timerTap, this); // called when tapped on the sprite
}

//the time text hasbeen tapped now kill it and increment gameTime
Widget.prototype.timerTap = function (sprite) {

    this.GameTime.counter += 6;
    // this.GameTime.timerText.setText(this.GameTime.counter);

    var text = this.game.add.text(sprite.x, sprite.y, '+' + 5, {
        font: "40px Courgette",
        fill: '#FFFFFF',
        strokeThickness: 10
    });
    text.anchor.setTo(0.5);
    text.scale.setTo(this.game.global.assetWidth, this.game.global.assetHeight);

    sprite.destroy(); // destroy this sprite

    var timeAnimation = this.game.add.tween(text).to({
        x: this.game.world.width - 90,
        y: this.game.world.height - 90,
        alpha: 0.5
    }, 700, Phaser.Easing.Exponential.In, true);

    timeAnimation.onComplete.add(function () {
        text.destroy();

    }, this);

    // used to check if it is tapped 
    this.timeUp = true;
}


Widget.prototype.render = function () {
    //console.log()
}