var ColorText = function (game) {
    this.aw = game.global.assetWidth;
    this.ah = game.global.assetHeight;
    this.game = game;
    this.texts = ['blue', 'brown', 'green', 'orange', 'purple', 'red', 'yellow'];
    this.colors = ['#017FF4', '#6C3A42', '#2EC522', '#FD600F', '#9B56F4', '#F5302D', '#FBD92B'];
    Phaser.ArrayUtils.shuffle(this.texts);
    Phaser.ArrayUtils.shuffle(this.colors);
    this.colortext = null;
}

ColorText.prototype.create = function (secs) {
    console.log('ColorText');
    this.createText(secs);

}

ColorText.prototype.createText = function (secs) {
    var rndText = this.game.rnd.integerInRange(0, this.texts.length - 1);
    var rndColor = this.game.rnd.integerInRange(0, this.colors.length - 1);
    var style = {
        font: "bold 60px Nunito",
    };
    this.colortext = this.game.add.text(this.game.world.centerX, this.game.world.centerY, this.texts[rndText], style);
    this.colortext.anchor.setTo(0.5, 0.5);
    this.colortext.scale.setTo(this.aw, this.ah);
    // loop events for colored text
    // every one sec the text and text color will be change
    this._timer = this.game.time.events.loop(Phaser.Timer.SECOND * secs, this.colorChange, this);;
}

ColorText.prototype.colorChange = function () {
    this.colortext.fill = this.colors[this.game.rnd.integerInRange(0, this.texts.length - 1)];
    this.colortext.text = this.texts[this.game.rnd.integerInRange(0, this.texts.length - 1)];

    //console.log('timer');
}