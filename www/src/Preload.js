Fuzzle.Preload = function (game) {
    this.game = game;
}


Fuzzle.Preload.prototype = {

    preload: function () {

        // add loadingbar background sprite
        this.loadingBg = this.add.sprite(this.world.centerX, this.world.centerY, 'loadingbarBg');
        // set anchor point to center
        this.loadingBg.anchor.setTo(0.5);
        // add loading bar 
        this.loadingBar = this.add.sprite(this.world.centerX, this.world.centerY, 'loadingbar');
        // set anchor point to right 
        this.loadingBar.anchor.setTo(0, 0.5);
        // set scale
        this.loadingBar.scale.setTo(0.5, 1);
        //
        this.loadingBar.x = this.world.centerX - this.loadingBar.width;
        // set preload sprite    
        this.load.setPreloadSprite(this.loadingBar);

        // load GUI sprite
        this.game.load.image('game-title', 'asset/GUI/game-title.png');
        this.game.load.image('play', 'asset/GUI/play.png');
        this.game.load.image('credit', 'asset/GUI/credit.png');
        this.game.load.image('back', 'asset/GUI/back.png');
        this.game.load.image('playagain', 'asset/GUI/playagain.png')

        //load tutorial assets
        this.game.load.image('howto_01', 'asset/howTo/howto_01.png');
        this.game.load.image('howto_02', 'asset/howTo/howto_02.png');

        // load game object sprites
        this.game.load.image('blue', 'asset/game_assets/blue.png');
        this.game.load.image('brown', 'asset/game_assets/brown.png');
        this.game.load.image('green', 'asset/game_assets/green.png');
        this.game.load.image('orange', 'asset/game_assets/orange.png');
        this.game.load.image('purple', 'asset/game_assets/purple.png');
        this.game.load.image('red', 'asset/game_assets/red.png');
        this.game.load.image('yellow', 'asset/game_assets/yellow.png');

        //load sfx
        this.game.load.audio('correctSound', ['sounds/ding.wav'], true);
        this.game.load.audio('tapSound', ['sounds/tap.wav'], true);
        this.game.load.audio('buttonTap', ['sounds/buttonTap.wav'], true);

    },

    create: function () {
        //console.log(this.game.state.getCurrentState());
        // forward the MainScreen when the sounds are decoded    
        this.game.sound.setDecodedCallback(['correctSound', 'tapSound'], function () {
            this.game.state.start('MainScreen');
        }, this);
    }

}