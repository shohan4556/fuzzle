Fuzzle.Credit = function (game) {
    this.game = game;
    this.text1 = null;
    this.text2 = null;
    this.aw = this.game.global.assetWidth;
    this.ah = this.game.global.assetHeight;
    this.textGroup = null;

    this.colorPalette = ['0xFF4E50'];
    this.textStyle = {
        font: "bold 30px Arvo", // loaded fonts : ['Revalia','Sniglet','Arvo','Open Sans']
        fill: '#FFFFFF',
        strokeThickness: 5
    };

    this.textStyle1 = {
        font: "bold 30px Kaushan Script",
        fill: '#FFFFFF',
        strokeThickness: 5
    };
}

Fuzzle.Credit.prototype.create = function () {
    console.log(this.game.state.getCurrentState());

    var centerX = this.world.centerX;
    var centerY = this.world.centerY;
    // random background color
    this.stage.backgroundColor = this.colorPalette[0];
    this.textGroup = this.add.group();
    // back to mainscreen
    this.backButton = this.game.add.button(centerX, this.game.height - 150, 'back', this.backToMenu, this);
    this.backButton.anchor.setTo(0.5, 0.5);
    this.backButton.scale.setTo(this.aw, this.ah);

    this.text1 = this.game.add.text(centerX, centerY, '\nDesigned & Developed by ,', this.textStyle);
    this.textGroup.add(this.text1);
    this.text1.alpha = 0;

    this.text2 = this.game.add.text(centerX, 20, '\nShohanur Rahaman ', this.textStyle1);
    this.textGroup.add(this.text2);

    this.textGroup.callAll('anchor.setTo', 'anchor', 0.5);
    this.textGroup.callAll('scale.setTo', 'scale', this.aw-0.2, this.ah-0.2);
    // tweening text1 and text2
    this.textTween(this.text1, this.text2);

}

Fuzzle.Credit.prototype.textTween = function (text1, text2) {
    var tween2 = this.add.tween(text2);
    tween2.to({
        y: this.world.centerY + this.text1.height
    }, 2000, Phaser.Easing.Bounce.Out, true);

    var tween1 = this.add.tween(text1);
    tween1.to({
        alpha: 1
    }, 2000, Phaser.Easing.Linear.None, true);
}

Fuzzle.Credit.prototype.backToMenu = function () {
    var buttonTap = this.game.add.audio('buttonTap',1,false);;
    buttonTap.play();
    this.game.state.start('MainScreen', Phaser.Plugin.StateTransition.Out.SlideBottom);

}