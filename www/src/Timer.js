var Timer = function (game) {
    this.game = game;
    this.counter = 0;
    this.timerText = null;
    this.gameTimer = null;
    this.timeCheck = false;
    this.aw = this.game.global.assetWidth;
    this.ah = this.game.global.assetHeight;
}

// counter - for seconds to kill ball
Timer.prototype.create = function (_counter) {
    this.counter = _counter;

    var scoreStyle = {
        font: "40px Oswald",
        fill: '#ED0B0F',
        strokeThickness: 10
    };
    this.timerText = this.game.add.text(this.game.world.width - 90, this.game.world.height - 90, _counter, scoreStyle);
    this.timerText.anchor.setTo(0.5, 0.5);
    this.timerText.scale.setTo(this.aw, this.ah);

    //timer
    this.gameTimer = this.game.time.create(true); // auto destroy true
    this.gameTimer.loop(Phaser.Timer.SECOND, this.timer, this); // looped after one second
}

// initial timer for
Timer.prototype.timer = function () {
    
    this.timerText.setText(this.counter); // add some tween later
    this.counter--;
    
    if (this.counter <= -1) {
        this.gameTimer.stop();
        this.gameTimer.destroy();
        // forward next step
        this.timeCheck = true; // time finished 
    } else {
        this.timeCheck = false; // time not finished
    }
}