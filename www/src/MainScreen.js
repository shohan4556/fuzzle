Fuzzle.MainScreen = function (game) {
    this.game = game;
    this.buttonTap = null; // hold the buttonTap sound
    // scale buttons 
    this.aw = this.game.global.assetWidth; // store game scale value
    this.ah = this.game.global.assetHeight; // store game scale value
    this.gameTitle = null;
}

Fuzzle.MainScreen.prototype = {

    create: function () {
        console.log(this.game.state.getCurrentState());

        var centerX = this.game.world.centerX; // store game world's center X coordinate value 
        var centerY = this.game.world.centerY; // store game world's center Y coordinate value 
        this.stage.backgroundColor = '#70337B'; // change the game background color to red

        this.buttonTap = this.game.add.audio('buttonTap',1,false); // buttonTap audio added volume 1 and loop false
        
        // added gametitle sprite
        this.gameTitle = this.add.sprite(centerX, this.game.width - centerY + 100, 'game-title');
        // set anchor point to center to sprite
        this.gameTitle.anchor.setTo(0.5); 
        // scale the gametitle with scaling variable (remember  app.js)
        this.gameTitle.scale.setTo(this.game.global.titleXY);
        
        // added play button
        this.playButton = this.game.add.button(centerX, centerY, 'play', this.handlePlay, this);
        // set anchor point to center to sprite
        this.playButton.anchor.setTo(0.5, 0.5);
        // scale play button
        this.playButton.scale.setTo(this.aw, this.ah);
        
        // added credit button
        this.creditButton = this.game.add.button(centerX, centerY + this.playButton.height + 50, 'credit', this.handleCredit, this);
        // set anchor point to center to sprite
        this.creditButton.anchor.setTo(0.5, 0.5);
        // scale button 
        this.creditButton.scale.setTo(this.aw, this.ah);
    },

    handlePlay: function () {
        this.buttonTap.play();
        
        this.game.state.clearCurrentState();
        this.game.state.start('HowTo', Phaser.Plugin.StateTransition.Out.SlideTop,this.buttonTap);
    },

    handleCredit: function () {
        // handle credit
        this.buttonTap.play();
        this.game.state.start('Credit', Phaser.Plugin.StateTransition.Out.SlideTop);
    }

}