(function () {
    // instanciate Phaser Game objct 
    var game = new Phaser.Game(window.innerWidth * window.devicePixelRatio, window.innerHeight * window.devicePixelRatio, Phaser.CANVAS, 'game');

    // game scaling variable 
    // assumed that default devicePixelRatio is 1
    game.global = {
        scaleRation: window.devicePixelRatio / 3, // for background scaling
        assetWidth: window.devicePixelRatio / 1, // game assets scaling variable
        assetHeight: window.devicePixelRatio / 1, // game assets scaling variable 
        ballWidth: window.devicePixelRatio / 2, // ball(game object) scaling variable 
        ballHeight: window.devicePixelRatio / 2, // 
        titleXY: window.devicePixelRatio / 2, // title scaling variable 
        howToXY: window.devicePixelRatio / 1, // howTo sprite scaling variable 
        GAME_SOUND: false, // 
        score: 0 // used to store score
    };

    // nothing complex according to device scaling my gameassets
    if (window.devicePixelRatio == 1.5) {
        game.global.assetWidth = window.devicePixelRatio - 0.5;
        game.global.assetHeight = window.devicePixelRatio - 0.5;
        game.global.titleXY = window.devicePixelRatio - 1;
        game.global.howToXY = window.devicePixelRatio / 1;
    } else if (window.devicePixelRatio >= 3) {
        game.global.assetWidth = window.devicePixelRatio - 1;
        game.global.assetHeight = window.devicePixelRatio - 1;
        game.global.howToXY = 2.5;
    }
    // scale the asset larger and bg same size
    else if (window.devicePixelRatio >= 2 && window.devicePixelRatio < 3) {
        game.global.assetWidth = window.devicePixelRatio;
        game.global.assetHeight = window.devicePixelRatio;
        game.global.ballWidth = window.devicePixelRatio - 1;
        game.global.ballHeight = window.devicePixelRatio - 1;
        //game.global.howToXY = window.devicePixelRatio-0.5;
    }

    game.state.add('Game', Fuzzle.Game);
    // preload state
    game.state.add('Preload', Fuzzle.Preload);
    //howto play state
    game.state.add('HowTo', Fuzzle.HowTo);
    // main game state
    game.state.add('Play', Fuzzle.Play);
    // MainScreen.js
    game.state.add('MainScreen', Fuzzle.MainScreen);
    // credit state
    game.state.add('Credit', Fuzzle.Credit);
    //score state
    game.state.add('Score', Fuzzle.Score);
    //  Now start the Game state.
    game.state.start('Game'); // bootState this state initiate the game screen and loading bar  

})();