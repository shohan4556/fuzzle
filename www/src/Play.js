Fuzzle.Play = function (game) {
    this.game = game;
    // instance variable
    this.GameTime = null; // for total game time 
    this.colorText = null;
    this.GameWidgets = null;

    this.aw = this.game.global.ballWidth;
    this.ah = this.game.global.ballHeight;
    this.names = ['blue', 'brown', 'green', 'orange', 'purple', 'red', 'yellow'];
    this.currentBallName = null;
    this.currentBall = null;
    this.BallGroup = null;
    this.flag = false;
    this.correctSound = null;
    this.tapSound = null;
    // score portion 
    this.score = 0; // global score/ current score
    this.scoreBuffer = 0;
    this.scoreLabel = null;
    this.scoreLabelTween = null;
}

Fuzzle.Play.prototype = {

        create: function () {

            // start physics system
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            // game background color
            var colorPalette = ['#FFFFFF', '#383355', '#CFBDF9', '#FFFFFF', '#EEFEF7'];
            this.game.stage.backgroundColor = colorPalette[this.game.rnd.integerInRange(0, colorPalette.length - 1)];

            // create ball - game object
            this.createBall();

            // randomColored  Text
            this.colorText = new ColorText(this.game);
            this.colorText.create(1); // 1 seconds
            // game Timer object
            this.GameTime = new Timer(this.game);
            this.GameTime.create(60); // you have 60s to go
            this.GameTime.gameTimer.start(); // true or false

            // score 
            this.createScore();

            //sound handle    
            this.manageSound();

            //widgets - time 
            this.GameWidgets = new Widget(this.game);
            this.GameWidgets.create(this.GameTime); // pass the GameTime reference

        },

    } // end prototype

Fuzzle.Play.prototype.manageSound = function () {
    this.correctSound = this.game.add.audio('correctSound', 1, false);
    this.tapSound = this.game.add.audio('tapSound', 1, false);
}

Fuzzle.Play.prototype.nextState = function () {
    // forward to Score State
    this.game.state.clearCurrentState(); // clear the current state 

    this.setScore();
    this.game.state.start('Score', Phaser.Plugin.StateTransition.Out.SlideBottom);
}

Fuzzle.Play.prototype.setScore = function () {
    this.game.global.score = this.score; // score set to global varibable 
    this.score = 0;
    this.scoreBuffer = 0;
}

Fuzzle.Play.prototype.update = function () {

        //this.game.physics.arcade.collide(this.BallGroup);

        if (this.flag == true && this.currentBallName === this.colorText.colortext.text) {
            console.log('matched');
            this.flag = false;
            this.currentBallName = null;
            this.tweenBall(this.currentBall);
            // increment score
            this.scoreAnimation(this.currentBall.x, this.currentBall.y, '+' + 5, 5);
        }
        if (this.flag == true && this.currentBallName != this.colorText.colortext.text) {
            console.log('not matched');
            this.scoreAnimation(this.currentBall.x, this.currentBall.y, '-' + 2, -2);
            this.flag = false;
        }
        this.currentBallName = null;
        this.currentBall = null;
        // score
        if (this.scoreBuffer > 0) {
            this.incrementScore();
            this.scoreBuffer--;
        } else if (this.scoreBuffer < 0) {
            this.decrementScore(2);
            this.scoreBuffer++;
        }

        //console.log(this.score);
        if (this.GameTime.timeCheck == true) // gametime finished here
            this.nextState(); // forword to score state

    } // end update

Fuzzle.Play.prototype.tweenBall = function (_ball) {
    // play sound
    this.correctSound.play();

    var myTween1 = this.game.add.tween(_ball);
    var rnd = this.game.rnd.integerInRange(0, 200);
    myTween1.to({
        x: this.game.world.centerX,
        y: this.game.world.centerY
    }, 500, Phaser.Easing.Cubic.InOut);
    myTween1.start();

}

Fuzzle.Play.prototype.createBall = function () {
    this.BallGroup = this.game.add.physicsGroup(Phaser.Physics.ARCADE);
    var randomX;
    var randomY;

    for (var i = 0; i < this.names.length; i++) {
        var ball = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, this.names[i], 0);
        ball.name = this.names[i];
        ball.anchor.setTo(0.5, 0.5);
        ball.scale.setTo(this.aw, this.ah);
        ball.inputEnabled = true;
        ball.input.useHandCursor = true;
        ball.events.onInputDown.add(this.ballSelect, this);
        // added to the physic property 
        this.BallGroup.add(ball);
        ball.body.mass = this.game.rnd.integerInRange(3, 10);
        ball.body.collideWorldBounds = true;
        ball.body.bounce.setTo(1, 1);

        if (window.devicePixelRatio <= 1.5) {
            randomX = this.game.rnd.integerInRange(-150, 150);
            randomY = this.game.rnd.integerInRange(-150, 150);
        } else {
            randomX = this.game.rnd.integerInRange(-250, 250);
            randomY = this.game.rnd.integerInRange(-250, 250);
        }
        ball.body.velocity.setTo(randomX, randomY); // set ball's velocity here
    }

}

Fuzzle.Play.prototype.ballSelect = function (ball) {
    // play sfx when a ball seleted 
    this.tapSound.play();

    this.currentBallName = ball.name;
    this.currentBall = ball;
    this.flag = true; // used to check if its selected or not
}

Fuzzle.Play.prototype.createScore = function () {
        var scoreStyle = {
            font: "40px Verdana",
            fill: '#FFFFFF',
            strokeThickness: 10
        };
        var textWidth = this.game.global.assetWidth;
        var textHeight = this.game.global.assetHeight;
        // create score label
        this.scoreLabel = this.game.add.text(0 + 90, 0 + 90, '0', scoreStyle);
        this.scoreLabel.anchor.setTo(0.5);
        this.scoreLabel.scale.setTo(textWidth, textHeight);
        this.scoreLabel.align = 'center';
        // create grow and shrink scorelabel
        this.scoreLabelTween = this.game.add.tween(this.scoreLabel.scale).to({
            x: textWidth,
            y: textHeight
        }, 200, Phaser.Easing.Linear.Out).to({
            x: textWidth + 0.2,
            y: textHeight + 0.2
        }, 200, Phaser.Easing.Bounce.Out);

    } // end score text

Fuzzle.Play.prototype.incrementScore = function () {
    //this.scoreLabelTween.start();
    this.score += 1;
    this.scoreLabel.text = this.score;
}

Fuzzle.Play.prototype.decrementScore = function (score) {
    this.score -= 1;
    this.scoreLabel.text = this.score;
}

//takes current ball axis and text 
Fuzzle.Play.prototype.scoreAnimation = function (xx, yy, text, score) {
        var scoreStyle = {
            font: "40px Courgette",
            fill: '#FFFFFF',
            strokeThickness: 10
        }
        var scoreAnim = this.game.add.text(xx, yy, text, scoreStyle);
        scoreAnim.anchor.setTo(0.5, 0);
        scoreAnim.scale.setTo(this.game.global.assetWidth, this.game.global.assetHeight);

        var scoreTween = this.game.add.tween(scoreAnim).to({
            x: 0 + 90,
            y: 0 + 90
        }, 800, Phaser.Easing.Exponential.In, true);

        scoreTween.onComplete.add(function () {
            scoreAnim.destroy();
            this.scoreLabelTween.start();
            this.scoreBuffer += score;
        }, this);
    } // end scoreanimation

Fuzzle.Play.prototype.render = function () {
    // for (var i = 0; i < this.BallGroup.length; i++) {
      //this.game.debug.body(this.BallGroup.children[i]);
    // }
}