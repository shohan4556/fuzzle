/* globals Phaser:false */
// create Fuzzle Class
Fuzzle = {

};

// using google webfont API
WebFontConfig = {
    active: function () {
        //console.log('font loaded');
    },
    //  The Google Fonts we want to load (specify as many as you like in the array)
    google: {
        families: ['Revalia', 'Roboto', 'Arvo', 'Courgette', 'Fredoka One', 'Indie Flower', 'Damion', 'Kaushan Script', 'Montserrat', 'Oswald', 'Nunito']
    }
};

// create Game function in Fuzzle
Fuzzle.Game = function (game) {
    this.game = game;
};

// set Game function prototype
Fuzzle.Game.prototype = {

    init: function () {
        // set up input max pointers
        this.input.maxPointers = 1;
        // set up stage disable visibility change
        // By default if the browser tab loses focus the game will pause. You can stop that behaviour by setting this property to true
        this.stage.disableVisibilityChange = true;
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL; // scaing mode SHOW_ALL
        this.scale.pageAlignHorizontally = true; // align horizontally 
        this.scale.pageAlignVertically = true; // align vertically
        this.scale.forceOrientation(false, true); // landscape - false, portrait - true, game only run on protrait mode
        this.scale.setMinMax(320, 480, 540, 860); // set minimum and maximum width and height 
        this.scale.setResizeCallback(this.gameResized, this);
        this.scale.updateLayout(true);
        // Re-calculate scale mode and update screen size. This only applies if
        // ScaleMode is not set to RESIZE.
        this.scale.refresh(); // refresh game scaling property 

    },

    preload: function () {

        // Here we load the assets required for our preloader (in this case a 
        // background and a loading bar)
        this.load.image('loadingbar', 'asset/GUI/loadingbar.png');
        this.load.image('loadingbarBg', 'asset/GUI/loadingbarBg.png');
        this.load.script('webfont', 'src/plugins/webfontloader.js'); // load google web font script
    },

    create: function () {
        // forward to preload state
        this.game.state.start('Preload');
    },

    gameResized: function (width, height) {
        // This could be handy if you need to do any extra processing if the 
        // game resizes. A resize could happen if for example swapping 
        // orientation on a device or resizing the browser window. Note that 
        // this callback is only really useful if you use a ScaleMode of RESIZE 
        // and place it inside your main game state.

    }

};