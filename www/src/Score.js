Fuzzle.Score = function (game) {
    this.game = game;
    this.textScore = null;
    this.textHighscore = null;
    this.aw = this.game.global.assetWidth;
    this.ah = this.game.global.assetHeight;
    this.textGroup = null;
    this.playAgainBtn = null;

    this.textStyle = {
        font: "35px Oswald",
        fill: '#00DFFC',
        strokeThickness: 10
    };

    this.textStyle1 = {
        font: "40px Oswald",
        fill: '#ED303C',
        strokeThickness: 10
    };
}

Fuzzle.Score.prototype.create = function () {
    console.log(this.game.state.getCurrentState());
    this.game.stage.backgroundColor = '#F9D423';

    this.textScore = this.game.add.text(this.game.world.centerX, 150, 'Your  Score', this.textStyle);
    this.textScore.anchor.setTo(0.5, 0.5);
    this.textScore.scale.setTo(this.aw, this.ah);

    this.textHighscore = this.game.add.text(this.game.world.centerX, this.textScore.width+150, 'Highscore', this.textStyle1);
    this.textHighscore.anchor.setTo(0.5);
    this.textHighscore.scale.setTo(this.aw, this.ah);

    // view score 
    this.myScore();

    this.playAgainBtn = this.game.add.button(this.game.world.centerX, this.game.height - 150, 'playagain', this.PlayAgain, this)
    this.playAgainBtn.anchor.setTo(0.5);
    this.playAgainBtn.scale.setTo(this.aw,this.ah);
}

Fuzzle.Score.prototype.PlayAgain = function () {
   this.game.state.start('Play',Phaser.Plugin.StateTransition.Out.SlideTop);
}

Fuzzle.Score.prototype.myScore = function () {
    var myScore = this.game.global.score; // current score 

    if (localStorage.getItem('score') === null) {
        localStorage.setItem('score', myScore);
    } else if (myScore > localStorage.getItem('score')) {
        localStorage.setItem('score', myScore); // highscore setted
    }
    var highscore = localStorage.getItem('score');

    var currentSCore = this.game.add.text(this.game.world.centerX, this.textScore.y + this.textScore.height + 25, myScore, this.textStyle);
    currentSCore.anchor.setTo(0.5, 0.5);
    currentSCore.scale.setTo(this.aw, this.ah);

    var highScore = this.game.add.text(this.game.world.centerX, this.textHighscore.y + this.textHighscore.height + 25, highscore, this.textStyle1);
    highScore.anchor.setTo(0.5, 0.5);
    highScore.scale.setTo(this.aw, this.ah);
    // console.log();
}